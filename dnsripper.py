#! /usr/bin/env python

import requests
import json
from urlparse import urlparse
from config import config


URL = 'https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Web?Query=%%27%(query)s%%27&Market=%%27en-US%%27&$top=50&$format=json'
API_KEY = config['api_key']

def remove_dups(seq, idfun=None): 
    # order preserving
    if idfun is None:
        def idfun(x): return x
    seen = {}
    result = []
    for item in seq:
        marker = idfun(item)
        # in old Python versions:
        # if seen.has_key(marker)
        # but in new ones:
        if marker in seen: continue
        seen[marker] = 1
        result.append(item)
    return result

def search(query, **params):
    r = requests.get(URL % {'query': query}, auth=(API_KEY, API_KEY))
    return json.loads(r.content)

def searchUrl(url):
    r = requests.get(url, auth=(API_KEY, API_KEY))
    return json.loads(r.content)

def getDomains(results):
	domains = []
	for result in results['d']['results']:
		domain = urlparse(result['Url']).hostname
		domains.append(domain)
	return domains


def dnsrecon(domain):
	query = 'site:%(domain)s NOT site:www.%(domain)s' % {'domain':domain}
	nextUrl = URL % {'query':query}
	domains = []
	while True:
		# Perform the search
		results = searchUrl(nextUrl)
		# Get the next search URL if available
		nextUrl = results.get('d').get('__next')
		# If the next URL is not empty add the JSON format parameter
		if nextUrl is not None:
			nextUrl += '&$format=json'
		# Get the list of domains from the results
		domains += getDomains(results)
		#for domain in domains:
		#	print domain
		if nextUrl is None:
			break


	domains = remove_dups(domains)
	domains.sort()

	return domains

domains = dnsrecon('microsoft.com')
for domain in domains:
	print domain

